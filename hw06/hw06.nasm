%define stdin 0
%define stdout 1
%define stderr 2

%define in_buffer_size (16*1024*1024)
%define depth_cap 128
%define max_n 500000

; For reference:
; sizeof(byte) = 1 (al)
; sizeof(word) = 2 (ax)
; sizeof(dword) = 4 (eax)
; sizeof(qword) = 8 (rax)

section .data
output_string: db `XXXXXXXXXX\n`
output_string_len: equ $-output_string

section .bss
in_buffer: resb in_buffer_size; [u8; in_buffer_size]
end_node: resd 1; u32
tourism_index: resd max_n; [u32; max_n]
visit_times: resd max_n; [u32; max_n]
edges_offsets: resd max_n; [u32; max_n]
edges: resd (max_n - 1) * 2 * 2; [u32; (max_n - 1) * 2 * 2]
lengths: resd max_n; [u32; max_n]
path: resd depth_cap; [u32; depth_cap]

section .text
global _start
_start:
  jmp main

dfs:
  ; eax ~ return value
  ; ebx ~ self
  ; ecx ~ child index
  ; edx ~ self_len
  ; edi ~ index in `edges`
  ; r8d ~ to
  ; r9d ~ cost
  ; r13d ~ remaining_time
  ; r14d ~ min_gain
  ; r15d ~ path_len
  mov ebx, [path + 4 * r15d - 4]
  cmp ebx, [end_node]
  je .end_success

  mov edx, [lengths + 4 * ebx]
  test edx, edx
  jz .end_fail

  mov edi, [edges_offsets + 4 * ebx]
  mov ecx, 0
  .child_iter:
    mov r8d, [edges + 4 * edi]
    mov r9d, [edges + 4 * edi + 4]
    mov r10d, [visit_times + 4 * r8d]
    add r9d, r10d
    cmp r8, [rsp + 8]; to == prev
    je .next_iter
    cmp r9d, r13d; cost > remaining_time
    jg .next_iter

    mov [path + 4 * r15d], r8d
    inc r15d
    sub r13d, r9d
    mov r10d, [tourism_index + 4 * r8d]
    add r14d, r10d
    push r9
    push r10
    push rdi
    push rdx
    push rcx
    push rbx
    call dfs
    pop rbx
    pop rcx
    pop rdx
    pop rdi
    pop r10
    pop r9
    test eax, eax
    jnz .end_success
    sub r14d, r10d
    add r13d, r9d
    dec r15d


  .next_iter:
    add edi, 2
    inc ecx
    cmp ecx, edx
    jne .child_iter

  .end_fail:
  mov eax, 0
  ret
  .end_success:
  mov eax, 1
  ret

read_advance:
  ; eax ~ ret
  ; r13d ~ 10 (multiplication constant)
  ; r14 ~ cursor pointer
  ; r15d ~ val
  mov eax, 0
  .nondigit:
    movzx r15d, byte [r14]
    sub r15b, '0'
    jl .nondigit_next
    cmp r15b, 9
    jle .digit

    .nondigit_next:
      inc r14
      jmp .nondigit

  .digit:
    mul r13d
    add eax, r15d
    inc r14
    movzx r15d, byte [r14]
    sub r15b, '0'
    jl .end
    cmp r15b, 9
    jg .end
    jmp .digit

  .end:
  ret

main:
  ; Read stdin
  mov rax, 0
  mov rdi, stdin
  mov rsi, in_buffer
  mov rdx, in_buffer_size
  syscall

  ; Load file header
  ; eax ~ val
  ; rcx ~ node count
  ; r14d ~ pointer to currently viewed byte
  mov r14, in_buffer
  mov r13, 10
  call read_advance
  mov ebx, eax
  call read_advance
  mov esi, eax
  call read_advance
  mov [end_node], eax
  call read_advance
  push rax

  ; Fill tourism_index
  ; eax ~ val
  ; rbx ~ current index
  ; rcx ~ node count
  mov rcx, 0
  .fill_tourism_index:
    call read_advance
    mov [tourism_index + 4 * rcx], eax
    inc rcx
    cmp rcx, rbx
    jne .fill_tourism_index

  ; Fill visit times
  ; eax ~ val
  ; rbx ~ node count
  ; rcx ~ current index
  mov rcx, 0
  .fill_visit_times:
    call read_advance
    mov [visit_times + 4 * rcx], eax
    inc rcx
    cmp rcx, rbx
    jne .fill_visit_times

  ; Start overwriting in_buffer
  mov r12, in_buffer
  ; Loop until N - 1
  dec rbx

  ; Fill number of edges from nodes, save edges to in_buffer
  ; r10 ~ from
  ; r11 ~ to
  mov rcx, 0
  .fill_lengths:
    ; from
    call read_advance
    mov r10d, eax
    mov [r12], eax

    ; to
    call read_advance
    mov r11d, eax
    mov [r12 + 4], eax

    ; cost
    call read_advance
    mov [r12 + 8], eax
    add r12, 12

    ; lengths
    inc dword [lengths + 4 * r10]
    inc dword [lengths + 4 * r11]

    inc rcx
    cmp rcx, rbx
    jne .fill_lengths

  ; Loop until N
  inc rbx

  mov rcx, 0
  mov r10, 0; current offset
  .fill_offsets:
    mov [edges_offsets + 4 * rcx], r10d
    mov r11d, [lengths + 4 * rcx]
    add r11d, r11d
    add r10d, r11d
    inc rcx
    cmp rcx, rbx
    jne .fill_offsets

  ; Clear lengths
  mov rcx, 0
  .clear_lengths:
    mov dword [lengths + 4 * rcx], 0
    inc rcx
    cmp rcx, rbx
    jne .clear_lengths


  ; Loop until N - 1
  dec rbx
  mov r13, in_buffer
  mov rcx, 0
  .fill_edges:
    mov r10d, [r13]; from
    mov r11d, [r13 + 4]; to
    mov r12d, [r13 + 8]; cost
    add r13, 12

    mov r14d, [edges_offsets + 4 * r10d]
    lea r15, [lengths + 4 * r10d]
    mov r8d, [r15]
    lea r9d, [r14 + 2 * r8]
    mov [edges + 4 * r9], r11d
    inc r9d
    mov [edges + 4 * r9], r12d
    inc dword [r15]

    mov r14d, [edges_offsets + 4 * r11d]
    lea r15, [lengths + 4 * r11d]
    mov r8d, [r15]
    lea r9d, [r14 + 2 * r8]
    mov [edges + 4 * r9], r10d
    inc r9d
    mov [edges + 4 * r9], r12d
    inc dword [r15]

    inc rcx
    cmp rcx, rbx
    jne .fill_edges

  ; Setup dfs
  mov r15, 1
  ; esi ~ start node
  mov dword [path], esi
  mov r14d, [tourism_index + 4 * esi]
  pop r13; remaining_time
  sub r13d, [visit_times + 4 * esi]

  jmp .end

  push -1
  call dfs
  mov [rsp], r14

  ; r13d ~ dp width - 1
  ; r15d ~ path len
  ; rbp ~ dp table
  ; ecx ~ path index
  ; r8d ~ backbone node
  ; edx ~ child index
  ; r12d ~ t
  ; r14d ~ max
  mov rbp, rsp
  lea r14, [4 * r13d + 4]
  sub rsp, r14


  mov r8, rsp
  .clear_dp:
    mov dword [r8], 0
    add r8, 4
    cmp r8, rbp
    jne .clear_dp

  xor rcx, rcx
  .dp_outer:
    mov r8d, [path + 4 * ecx]
    mov r9d, [lengths + 4 * r8d]
    mov edx, 0
    mov edi, [edges_offsets + 4 * r8d]
    .dp_inner:
      mov r10d, [edges + 4 * edi]
      mov r12d, [visit_times + 4 * r10d]
      mov r11d, [edges + 4 * edi + 4]
      lea r11d, [2 * r11d]
      add r11d, r12d
      test ecx, ecx
      je .post_prev_check
      cmp r10d, [path + 4 * ecx - 4]
      je .next_iter
      .post_prev_check:
      lea r12d, [r15d - 1]
      cmp ecx, r12d
      je .post_post_check
      cmp r10d, [path + 4 * ecx + 4]
      je .next_iter
      .post_post_check:

      ;lea r12d, [r13d - 1]
      mov r12d, r13d
      .dp_time:
        cmp r12d, r11d
        jl .next_iter
        mov r14d, [tourism_index + 4 * r10d]
        mov rsi, r12
        sub rsi, r11
        add r14d, [rsp + 4 * rsi]
        cmp r14d, [rsp + 4 * r12]
        cmovl r14d, [rsp + 4 * r12]
        mov [rsp + 4 * r12], r14d
        dec r12d
        jmp .dp_time

      .next_iter:
      add edi, 2
      inc edx
      cmp edx, r9d
      jne .dp_inner
    inc ecx
    cmp ecx, r15d
    jne .dp_outer

  mov eax, [rbp]
  add eax, [rbp - 4]
  mov rsp, rbp

  ; result in eax
  mov esi, 10 ; mutliplier
  lea ecx, [output_string + output_string_len - 2]
  .print_loop:
    mov edx, 0
    div esi ; puts quotient into edx
    add edx, '0'
    mov [ecx], dl
    dec ecx
    test eax, eax
    jnz .print_loop
  inc rcx

  mov rax, 1
  mov rdi, stdout
  mov rsi, rcx
  mov rbx, rcx
  sub rbx, output_string
  mov rdx, output_string_len
  sub rdx, rbx
  syscall

.end:
  mov rax, 0x3c
  mov rdi, 0
  syscall
