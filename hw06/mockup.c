#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEPTH_CAP 100
#define MAX_N 500000

#define IN_BUFFER_SIZE 16 * 1024 * 1024
char in_buffer[IN_BUFFER_SIZE];
size_t in_buffer_read;
char *cursor = in_buffer;

unsigned target;
/* size_t *edges_offsets; */
size_t edges_offsets[MAX_N];
unsigned edges[(MAX_N - 1) * 2 * 2];
unsigned lengths[MAX_N];
unsigned path[DEPTH_CAP];
size_t path_len;
unsigned remaining_time;
unsigned min_gain;
unsigned visit_times[MAX_N];
unsigned tourism_index[MAX_N];

int dfs(unsigned prev)
{
    unsigned self = path[path_len - 1];
    unsigned self_len = lengths[self];
    if (self == target)
    {
        return 1;
    }
    if (self_len == 0)
    {
        return 0;
    }
    size_t index = edges_offsets[self];
    for (unsigned i = 0; i < self_len; i++)
    {
        unsigned to = edges[index];
        unsigned cost = edges[index + 1] + visit_times[to];
        if (to == prev)
        {
            goto next_iter;
        }
        if (remaining_time < cost)
        {
            goto next_iter;
        }
        path[path_len++] = to;
        remaining_time -= cost;
        min_gain += tourism_index[to];
        if (dfs(self))
        {
            return 1;
        }
        min_gain -= tourism_index[to];
        remaining_time += cost;
        path_len--;
        /* ----------------------------------------------- */
    next_iter:
        index += 2;
    }
    return 0;
}

inline int is_num(char c)
{
    return c >= '0' && c <= '9';
}

unsigned read_advance()
{
    unsigned ret = 0;
    while (!is_num(*cursor))
    {
        cursor++;
    }
    while (is_num(*cursor))
    {
        ret *= 10;
        ret += *(cursor++) - '0';
    }

    return ret;
}

int main()
{
    unsigned N, A;
    in_buffer_read = fread_unlocked(in_buffer, 1, IN_BUFFER_SIZE, stdin);
    N = read_advance();
    A = read_advance();
    target = read_advance();
    remaining_time = read_advance();

    for (unsigned i = 0; i < N; i++)
    {
        tourism_index[i] = read_advance();
    }
    for (unsigned i = 0; i < N; i++)
    {
        visit_times[i] = read_advance();
    }

    unsigned *read_cursor = (unsigned *)in_buffer;
    for (unsigned i = 0; i < N - 1; i++)
    {
        // Load lengths
        unsigned from = read_advance();
        *(read_cursor++) = from;
        unsigned to = read_advance();
        *(read_cursor++) = to;
        *(read_cursor++) = read_advance();
        lengths[from]++;
        lengths[to]++;
    }
    size_t cur_offset = 0;
    for (unsigned i = 0; i < N; i++)
    {
        // distribute edge offsets
        edges_offsets[i] = cur_offset;
        cur_offset += lengths[i] * 2;
    }

    unsigned _from, _to, _cost;
    memset(lengths, 0, N * sizeof(unsigned));
    read_cursor = (unsigned *)in_buffer;
    for (unsigned i = 0; i < N - 1; i++)
    {
        _from = *(read_cursor++);
        _to = *(read_cursor++);
        _cost = *(read_cursor++);
        unsigned from_offset = edges_offsets[_from];
        unsigned *from_length = lengths + _from;
        edges[from_offset + (*from_length * 2)] = _to;
        edges[from_offset + (*from_length * 2) + 1] = _cost;
        (*from_length)++;
        from_offset = edges_offsets[_to];
        from_length = lengths + _to;
        edges[from_offset + (*from_length * 2)] = _from;
        edges[from_offset + (*from_length * 2) + 1] = _cost;
        (*from_length)++;
    }

    // find shortest path
    path_len = 1;
    path[0] = A;
    min_gain = tourism_index[A];
    remaining_time -= visit_times[A];
    dfs((unsigned)-1);

    // do the dynamic stuff
    unsigned dp_width = remaining_time + 1;
    unsigned dp[dp_width];
    memset(dp, 0, dp_width * sizeof(unsigned));
    /* unsigned *dp = alloca(dp_width * dp_height * sizeof(unsigned)); */

    for (unsigned i = 0; i < path_len; i++)
    {
        unsigned from = path[i];
        unsigned self_len = lengths[from];
        size_t index = edges_offsets[from];
        for (unsigned j = 0; j < self_len; j++)
        {
            unsigned to = edges[index];
            unsigned required_time = 2 * edges[index + 1] + visit_times[to];
            if (i != 0 && path[i - 1] == to)
            {
                goto next_iter;
            }
            if (i != path_len - 1 && path[i + 1] == to)
            {
                goto next_iter;
            }

            for (int t = dp_width - 1; t >= required_time; t--)
            {
                /* ---------------------------------------- */
                unsigned taken = tourism_index[to];
                taken += dp[t - required_time];
                if (taken < dp[t])
                {
                    taken = dp[t];
                }
                dp[t] = taken;
            }
        next_iter:
            index += 2;
        }
    }
    printf("%u\n", dp[dp_width - 1] + min_gain);
}
