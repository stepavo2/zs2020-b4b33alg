#!/usr/bin/env crystal

require "math"

EDGE_START_CAP = 5

alias TownID = UInt16

struct Edge
  getter from
  getter to
  getter cost
  getter tsoc

  def initialize(@from : TownID, @to : TownID, @cost : UInt32, @tsoc : UInt32)
  end
end

town_count, edge_count, center_count, prescribed_visits, prescribed_centers =
  (gets || raise "Failed to open stdin").split(' ').map &.to_i

edges = Array(Edge).new edge_count

edge_count.times do
  a, b, cab, cba = (gets || raise "Unsufficient edges").split(' ').map &.to_u32
  edges << Edge.new a.to_u16, b.to_u16, cab, cba
end

centers = Array.new town_count, false

(gets || raise "Centers not provided").split(' ').map(&.to_u16).each do |i|
  centers[i] = true
end

prev = Array.new town_count do |start|
  fill = Array.new prescribed_centers + 1, -1
  fill[centers[start] ? 1 : 0] = 0
  fill
end
cur = Array.new town_count do
  Array.new prescribed_centers + 1, -1
end

(prescribed_visits - 1).times do
  # puts prev
  edges.each do |e|
    src = prev[e.to]
    dest = cur[e.from]
    offset = centers[e.from] ? 1 : 0
    dest.map_with_index! do |prev_val, i|
      next prev_val if i - offset == -1
      next prev_val if src[i - offset] == -1
      Math.max(prev_val, e.cost.to_i + src[i - offset])
    end
    src = prev[e.from]
    dest = cur[e.to]
    offset = centers[e.to] ? 1 : 0
    dest.map_with_index! do |prev_val, i|
      next prev_val if i - offset == -1
      next prev_val if src[i - offset] == -1
      Math.max(prev_val, e.tsoc.to_i + src[i - offset])
    end
  end
  cur, prev = prev, cur
end

global_max = 0
prev.each do |a|
  global_max = Math.max(global_max, a[prescribed_centers])
end

puts global_max
