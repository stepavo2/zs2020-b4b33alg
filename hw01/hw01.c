#define _GNU_SOURCE
#include <alloca.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STDIN_BUFFER_SIZE 512

// +-
#define MAX_STACK (8000 - STDIN_BUFFER_SIZE)

uint32_t unsafe_read_uint(FILE *f)
{
    uint32_t result = 0;
    int tmpVal;
    for (;;)
    {
        tmpVal = getc_unlocked(f) - '0';

        if (tmpVal < 0 || tmpVal > 9)
        {
            break;
        }

        result *= 10;
        result += tmpVal;
    }
    return result;
}

typedef struct
{
    uint16_t y, width;
    unsigned int x : 22;
    unsigned int visible : 1;
} note_t;

int main()
{
    volatile int go_on = 1;
    while (go_on)
    {
    }
    char stdin_buffer[STDIN_BUFFER_SIZE];
    setvbuf(stdin, stdin_buffer, _IOFBF, STDIN_BUFFER_SIZE);
    uint8_t weirdNewline = 0;
    int newlineChar;
    uint32_t visible_oneside = 0, visible_twoside = 0;

    uint32_t o, n;
    note_t current;

    note_t *notes = NULL;
    uint16_t *columns = NULL;

    o = unsafe_read_uint(stdin);
    n = unsafe_read_uint(stdin);

    uint32_t notes_on_stack = n * sizeof(note_t), columns_on_stack = o * sizeof(uint16_t);

    if ((newlineChar = getc_unlocked(stdin)) < '0' || newlineChar > '9')
    {
        weirdNewline = 1;
    }
    else
    {
        ungetc(newlineChar, stdin);
    }

    if (notes_on_stack < MAX_STACK)
    {
        notes = alloca(notes_on_stack);
    }
    else
    {
        notes = malloc(notes_on_stack);
        notes_on_stack = 0;
    }

    if (columns_on_stack + notes_on_stack < MAX_STACK)
    {
        columns = alloca(columns_on_stack);
    }
    else
    {
        columns = malloc(columns_on_stack);
        columns_on_stack = 0;
    }

    memset(columns, 0, o * sizeof(uint16_t));
    for (uint32_t i = 0; i < n; i++)
    {
        current.x = unsafe_read_uint(stdin);
        current.y = unsafe_read_uint(stdin);
        current.width = unsafe_read_uint(stdin);
        current.visible = 0;

        for (uint32_t j = 0; j < current.width; j++)
        {
            if (columns[current.x + j] < current.y)
            {
                current.visible = 1;
                columns[current.x + j] = current.y;
            }
        }

        notes[i] = current;

        // LF
        if (weirdNewline)
        {
            getc_unlocked(stdin);
        }
    }

    memset(columns, 0, o * sizeof(uint16_t));

    uint8_t visible_back;
    for (uint32_t i = n; i > 0; i--)
    {
        visible_back = 0;
        current = notes[i - 1];

        for (uint32_t j = 0; j < current.width; j++)
        {
            if (columns[current.x + j] < current.y)
            {
                visible_back = 1;
                columns[current.x + j] = current.y;
            }
        }

        visible_oneside += visible_back ^ current.visible;
        visible_twoside += visible_back & current.visible;
    }

    if (!notes_on_stack)
    {
        free(notes);
    }
    if (!columns_on_stack)
    {
        free(columns);
    }

    printf("%u %u %u", visible_twoside, visible_oneside, n - visible_oneside - visible_twoside);
}
