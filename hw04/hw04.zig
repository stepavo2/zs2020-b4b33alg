const std = @import("std");
const io = std.io;
const parseInt = std.fmt.parseInt;
const assert = std.debug.assert;

const TailQueue = std.TailQueue;
const Buffer = std.Buffer;
const ArrayList = std.ArrayList;

const Position = struct {
    x: i32,
    y: i32,
};

const Step = struct {
    pos: Position,
    energy: i32,
    distance: usize,
};

const dxs = [_]i32{ 1, -1, 0, 0 };
const dys = [_]i32{ 0, 0, 1, -1 };
const dirs = [_]i32{ -1, 0, 1 };

var rows: i32 = undefined;
var cols: i32 = undefined;
var d_mov: i32 = undefined;
var d_env: i32 = undefined;
var grid: ArrayList(i32) = undefined;
var visited_with_energy: ArrayList(i32) = undefined;
var q: TailQueue(Step) = TailQueue(Step).init();

fn signum(comptime T: type, a: T) i8 {
    if (a > 0) {
        return 1;
    }
    if (a < 0) {
        return -1;
    }
    return 0;
}

fn parseLine(comptime T: type, target: []T, buf: *Buffer) !void {
    const line = try io.readLine(buf);
    var tokens = std.mem.tokenize(line, " ");
    for (target) |*slot, i| {
        slot.* = try parseInt(T, tokens.next().?, 10);
    }
}

fn pos_to_idx(pos: Position) usize {
    return @intCast(usize, pos.y * cols + pos.x);
}

fn ion_at(pos: Position) i32 {
    return grid.at(pos_to_idx(pos));
}

fn energy_at(pos: Position) *i32 {
    return &visited_with_energy.toSlice()[pos_to_idx(pos)];
}

fn compute_energy(cur_e: i32, cur_i: i32, next_i: i32) i32 {
    const middle_e = std.math.max(0, cur_e + signum(i32, next_i - cur_i) * d_mov);
    const end_e = if (middle_e > next_i) std.math.max(next_i, middle_e - d_env) else middle_e;
    assert(end_e >= 0);
    return end_e;
}

fn queue_from(current: Step, allocator: *std.mem.Allocator) !void {
    if (current.energy == 0) {
        return;
    }

    var move: usize = 0;
    var next_steps: [4]Step = undefined;
    var step_count: u32 = 0;
    while (move < 4) : (move += 1) {
        const new_x = current.pos.x + dxs[move];
        const new_y = current.pos.y + dys[move];
        if (new_x < 0 or new_x >= cols or new_y < 0 or new_y >= rows) {
            continue;
        }

        const new_pos = Position{
            .x = new_x,
            .y = new_y,
        };

        if (new_x == cols - 1 and new_y == rows - 1) {
            const node = try q.createNode(Step{
                .pos = new_pos,
                .distance = current.distance + 1,
                .energy = 0,
            }, allocator);
            q.prepend(node);
            return;
        }

        const new_energy = compute_energy(current.energy, ion_at(current.pos), ion_at(new_pos));
        const prev_energy = energy_at(new_pos);

        if (prev_energy.* >= new_energy) {
            continue;
        }

        prev_energy.* = new_energy;

        next_steps[step_count] = Step{
            .pos = new_pos,
            .energy = new_energy,
            .distance = current.distance + 1,
        };
        step_count += 1;
    }

    for (next_steps[0..step_count]) |step| {
        const node = try q.createNode(step, allocator);
        q.append(node);
    }
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.direct_allocator);
    defer arena.deinit();
    const allocator = &arena.allocator;

    var stdin_buf = try Buffer.initSize(allocator, 0);
    defer stdin_buf.deinit();

    var header_buf: [5]i32 = undefined;
    try parseLine(i32, &header_buf, &stdin_buf);

    rows = header_buf[0];
    cols = header_buf[1];
    const energy_init = header_buf[2];
    d_mov = header_buf[3];
    d_env = header_buf[4];

    grid = ArrayList(i32).init(allocator);

    visited_with_energy = ArrayList(i32).init(allocator);

    try grid.resize(@intCast(usize, cols * rows));
    try visited_with_energy.resize(@intCast(usize, cols * rows));

    std.mem.set(i32, visited_with_energy.toSlice(), 0);

    var row: i32 = 0;
    while (row < rows) : (row += 1) {
        // stdin_buf.shrink(0);
        var rowSlice = grid.toSlice()[@intCast(usize, row * cols)..@intCast(usize, (row + 1) * cols)];
        try parseLine(i32, rowSlice, &stdin_buf);
    }

    try queue_from(Step{
        .energy = energy_init,
        .pos = Position{
            .x = 0,
            .y = 0,
        },
        .distance = 0,
    }, allocator);

    while (q.popFirst()) |top| {
        if (top.data.pos.x == cols - 1 and top.data.pos.y == row - 1) {
            try (try io.getStdOut()).outStream().stream.print("{}\n", top.data.distance);
            return;
        }
        try queue_from(top.data, allocator);
        // q.destroyNode(top, allocator);
    }
    std.debug.warn("No path found\n");
}
