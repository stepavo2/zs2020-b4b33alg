The point of this repository is to try and implement the assignments from the B4B33ALG course in various languages.

So far, some of the Makefiles are outdated and the folder structure is a subject to change.
I will try to provide Dockerfiles, so that you don't need to install any compilers or runtimes.

