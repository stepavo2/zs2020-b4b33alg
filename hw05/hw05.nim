from strutils import split, parseInt
import math
from sequtils import map, foldl

type
  Node = ref object
    left, right: Node
    val: Positive
  Tree = ref object
    root: Node
    depth, count: Natural

func emptyTree(): Tree =
  return Tree(root: nil, depth: 0, count: 0)

func `$`(node: Node): string =
  if node == nil:
    return ""
  else:
    return " " & $node.val & $node.left & $node.right

func nodeDepth(node: Node): range[-1..high(int)] =
  if node == nil or (node.left == nil and node.right == nil):
    return 0
  return 1 + max(nodeDepth(node.left), nodeDepth(node.right))

func shouldConsolidate(t: Tree): bool = 2 ^ t.depth > 2 * t.count

proc nodePopBiggest(node: var Node): int =
  if node == nil:
    return -1

  if node.right == nil:
    result = node.val
    node = node.left
    return

  return nodePopBiggest(node.right)

func fullSize(size: int): int =
  result = 1
  while result <= size:
    result += result + 1
  return result div 2

proc insertNode(node: var Node, val: Positive): int =
    if node == nil:
      # node = Node(left: nil, right: nil, val: val)
      # new(node)
      node = new(Node)
      node.val = val
      return 0

    if val > node.val:
      let insertedAt = insertNode(node.right, val)
      return if insertedAt == -1: -1 else: 1 + insertedAt

    if val < node.val:
      let insertedAt = insertNode(node.left, val)
      return if insertedAt == -1: -1 else: 1 + insertedAt

    return -1

proc deleteNode(node: var Node, val: Positive): bool =
  if node == nil:
    return false

  if val > node.val:
    return deleteNode(node.right, val)

  if val < node.val:
    return deleteNode(node.left, val)

  if node.left == nil and node.right == nil:
    node = nil
    return true

  if node.left == nil and node.right != nil:
    node = node.right
    return true

  if node.left != nil and node.right == nil:
    node = node.left
    return true

  node.val = nodePopBiggest(node.left)
  return true

proc nodeToVine(node: var Node) =
  var
    root = node
    remainder = node.right
  while remainder != nil:
    if remainder.left == nil:
      root = remainder
      remainder = remainder.right
    else:
      let tmp = remainder.left
      remainder.left = tmp.right
      tmp.right = remainder
      remainder = tmp
      root.right = tmp

proc nodeCompress(node: var Node, count: Natural): void =
  var scanner = node
  for i in 0..count - 1:
    var child = scanner.right
    scanner.right = child.right
    scanner = scanner.right
    child.right = scanner.left
    scanner.left = child

proc vineToTree(node: var Node, count: Natural): void =
  let full = fullSize(count)
  nodeCompress(node, count - full)
  var s = full
  while s > 1:
    nodeCompress(node, s div 2)
    s = s div 2

proc consolidate(t: var Tree) =
  # var pseudoRoot: Node = Node(left: nil, right: t.root, val: 1)
  var pseudoRoot: Node = new(Node)
  # new(pseudoRoot)
  pseudoRoot.val = 1
  pseudoRoot.left = nil
  pseudoRoot.right = t.root
  nodeToVine(pseudoRoot)
  vineToTree(pseudoRoot, t.count)
  t.root = pseudoRoot.right
  t.depth = toInt(log2(toFloat(t.count)) - 0.5)

proc applyOp(t: var Tree, op: int): bool =
  result = false
  if t == nil:
    t = emptyTree()
  if op > 0:
    let insertedAt = insertNode(t.root, op)
    if insertedAt != -1:
      t.count += 1;
      t.depth = max(t.depth, insertedAt)
      if shouldConsolidate(t):
        echo "depth: ", t.depth, ", count: ", t.count
        consolidate(t)
        return true
      return false

  elif op < 0:
    let deleted = deleteNode(t.root, -op)
    if deleted:
      t.count -= 1
      t.depth = nodeDepth(t.root)
      if shouldConsolidate(t):
        echo "depth: ", t.depth, ", count: ", t.count
        consolidate(t)
        return true
      return false


proc applyOps(ops: seq[int]): (Natural, Tree) =
  var tree: Tree = nil
  var consolidations: Natural = 0
  for i, op in ops:
    let consolidated = applyOp(tree, op)
    if consolidated:
      echo "Consolidated after operation at index ", i, ": ", op
      echo ""
    consolidations += int(consolidated)
  return (consolidations, tree)

proc main() =
  discard readLine(stdin)
  let ops = readLine(stdin).split(" ").map(parseInt)
  let (consolidations, tree) = applyOps(ops)
  echo consolidations, " ", tree.depth

main()
