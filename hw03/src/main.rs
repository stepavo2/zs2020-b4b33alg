use std::io::{BufRead, BufReader};

fn get_running_sums(tree: &[u32]) -> Vec<isize> {
    let mut running_weights = Vec::<isize>::with_capacity(tree.len());

    let mut s = 0isize;
    for &val in tree {
        s += val as isize;
        running_weights.push(s);
    }

    running_weights
}

fn diff_partial(s: &[isize], pivot: isize) -> isize {
    let fixed_left = if pivot == 0 { 0 } else { s[pivot as usize - 1] };
    let sum_left = fixed_left;
    let sum_right = s[s.len() - 1] - fixed_left;

    (sum_left - sum_right).abs()
}

fn find_pivot_partials(s: &[isize]) -> isize {
    match s.len() {
        0 => panic!("rawr"),
        1 => 0,
        2 => 1,
        l => {
            let mut pivot = l as isize / 2;

            let mut diff = diff_partial(s, pivot);
            let mut diff_shifted = diff_partial(s, pivot - 1);

            if diff == diff_shifted {
                return pivot - 1;
            }

            let dir = if diff_shifted < diff { -1 } else { 1 };

            loop {
                pivot += dir;
                diff_shifted = diff;
                diff = diff_partial(s, pivot);

                if diff == diff_shifted && dir == -1 {
                    break pivot;
                }
                if diff >= diff_shifted {
                    break (pivot - dir);
                }
            }
        }
    }
}

type PilotInfluence = isize;

fn place_weight(tree: &[u32], w: u32) -> (isize, PilotInfluence, PilotInfluence) {
    if tree.len() == 1 {
        return Default::default();
    }

    let (left, right) = get_subtree(tree);
    let (new_left_imbalance, left1, left2) = place_weight(left, w);
    let (new_right_imbalance, right1, right2) = place_weight(right, w);
    let w = w as isize;

    let running_weights = get_running_sums(tree);

    let pivot = find_pivot_partials(&running_weights);
    let fixed_bottom = if pivot == 0 {
        0
    } else {
        running_weights[pivot as usize - 1]
    } as isize;
    let mut left_weight = fixed_bottom;
    let mut right_weight = running_weights[running_weights.len() - 1] as isize - fixed_bottom;

    let imbalance = (left_weight - right_weight).abs();

    let new_imbalance = imbalance + new_right_imbalance + new_left_imbalance;

    let left1diff = left1 + (left_weight + w - right_weight).abs() - imbalance;
    let right1diff = right1 + (left_weight - (right_weight + w)).abs() - imbalance;

    if left1diff <= right1diff {
        // Definitely placing left1
        left_weight += w;
        let imbalance = (left_weight - right_weight).abs();
        let left2diff = left2 + (left_weight + w - right_weight).abs() - imbalance;
        let right1diff = right1 + (left_weight - (right_weight + w)).abs() - imbalance;
        if left2diff <= right1diff {
            (new_imbalance, left1diff, left2diff)
        } else {
            (new_imbalance, left1diff, right1diff)
        }
    } else {
        // Definitely placing right1
        right_weight += w;
        let imbalance = (left_weight - right_weight).abs();
        let left1diff = left1 + (left_weight + w - right_weight).abs() - imbalance;
        let right2diff = right2 + (left_weight - (right_weight + w)).abs() - imbalance;
        if left1diff <= right2diff {
            (new_imbalance, right1diff, left1diff)
        } else {
            // Placing right1 and left1
            (new_imbalance, right1diff, right2diff)
        }
    }
}

fn get_subtree(tree: &[u32]) -> (&[u32], &[u32]) {
    if tree.len() == 2 {
        return (&tree[..1], &tree[1..2]);
    }

    let running_weights = get_running_sums(tree);

    let pivot = find_pivot_partials(&running_weights);

    (&tree[..pivot as usize], &tree[pivot as usize..])
}

fn main() {
    let stdin = std::io::stdin();
    let handle = stdin.lock();
    let reader = BufReader::new(handle);
    let mut lines = reader.lines();

    let p = if let Some(Ok(line)) = lines.next() {
        line.split(' ')
            .map(str::parse::<u32>)
            .map(Result::unwrap)
            .take(2)
            .collect::<Vec<_>>()[1]
    } else {
        panic!("Ruh Rah")
    };

    let planes = lines
        .next()
        .unwrap()
        .unwrap()
        .split(' ')
        .map(str::parse::<u32>)
        .map(Result::unwrap)
        .collect::<Vec<_>>();

    let (total, a, b) = place_weight(&planes, p);

    println!("{} {}", total, total + a + b);
}
