#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define COMPAT(x1, x2) ((x1 & x2) == 0)

char stdin_buffer[STDIN_BUFFER_SIZE];

inline void setup_input()
{
  setvbuf(stdin, stdin_buffer, _IOFBF, STDIN_BUFFER_SIZE);
}

uint32_t unsafe_read_uint(FILE *f)
{
  uint32_t result = 0;
  int tmpVal;
  for (;;)
  {
    tmpVal = getc_unlocked(f) - '0';

    if (tmpVal < 0 || tmpVal > 9)
    {
      break;
    }

    result *= 10;
    result += tmpVal;
  }
  return result;
}

typedef struct
{
  uint64_t days;
  uint32_t gain;
} e_t;

e_t *end;

#ifdef DEBUG
e_t *global_start;

char hex[] = {
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
};
#endif

#ifdef DEBUG
void solve(e_t *start, uint32_t *out_gain, uint64_t *out_lab, uint64_t prev_lab, uint8_t days, char *output)
#else
void solve(e_t *start, uint32_t *out_gain, uint8_t *out_lab, uint64_t prev_lab, uint8_t days)
#endif
{
  e_t me = *start;
  uint32_t max_gain = 0;
  uint8_t max_lab = __builtin_popcountl(prev_lab);
#ifdef DEBUG
  char max_out[days];
  memset(max_out, 0, days);
#endif
  for (uint8_t shift = 0;; shift++)
  {
    uint64_t activity = me.days << shift;
    if (activity > (((uint64_t)1 << days) - 1))
    {
      break;
    }

    if (!COMPAT(prev_lab, activity))
    {
      continue;
    }

    if (me.gain > max_gain)
    {
      max_gain = me.gain;
      max_lab += __builtin_popcountl(activity);
#ifdef DEBUG
      for (uint8_t i = 0; i < days; i++)
      {
        if (activity & ((uint64_t)1 << i))
        {
          max_out[i] = hex[((void *)start - (void *)global_start) / sizeof(e_t)];
        }
      }
#endif
    }

    for (e_t *minion = start + 1; minion != end; minion++)
    {
      uint32_t new_gain;
      uint8_t new_lab;
#ifdef DEBUG
      char lab_output[days];
      memset(lab_output, 0, days);
      solve(minion, &new_gain, &new_lab, prev_lab | activity, days, lab_output);
#else
      solve(minion, &new_gain, &new_lab, prev_lab | activity, days);
#endif
      new_gain += me.gain;
      if (new_gain > max_gain)
      {
        max_gain = new_gain;
        max_lab = new_lab;
#ifdef DEBUG
        memset(max_out, 0, days);
        for (uint8_t i = 0; i < days; i++)
        {
          if (activity & ((uint64_t)1 << i))
          {
            max_out[i] = hex[((void *)start - (void *)global_start) / sizeof(e_t)];
          }
        }
        for (uint8_t i = 0; i < days; i++)
        {
          if (lab_output[i])
          {
            max_out[i] = lab_output[i];
          }
        }
#endif
      }
      else if (new_gain == max_gain)
      {
        if (new_lab > max_lab)
        {
          max_lab = new_lab;
#ifdef DEBUG
          memset(max_out, 0, days);
          for (uint8_t i = 0; i < days; i++)
          {
            if (activity & ((uint64_t)1 << i))
            {
              max_out[i] = hex[((void *)start - (void *)global_start) / sizeof(e_t)];
            }
          }
          for (uint8_t i = 0; i < days; i++)
          {
            if (lab_output[i])
            {
              max_out[i] = lab_output[i];
            }
          }
#endif
        }
      }
    }
  }
#ifdef DEBUG
  for (uint8_t i = 0; i < days; i++)
  {
    if (max_out[i])
    {
      output[i] = max_out[i];
    }
  }
#endif
  *out_gain = max_gain;
  *out_lab = max_lab;
}

int main()
{
  uint8_t N, Dmax;
  N = unsafe_read_uint(stdin);
  Dmax = unsafe_read_uint(stdin);

  e_t exps[N];
  end = exps + N;
#ifdef DEBUG
  global_start = exps;
#endif
  e_t current;

  for (uint8_t i = 0; i < N; i++)
  {
    uint8_t active = unsafe_read_uint(stdin);
    current.gain = unsafe_read_uint(stdin);
    current.days = 0;
    for (uint8_t j = 0; j < active; j++)
    {
      current.days |= ((uint64_t)1 << unsafe_read_uint(stdin));
    }

    exps[i] = current;
  }

  uint32_t max_gain = 0;
  uint8_t max_lab = 0;

#ifdef DEBUG
  char output[Dmax + 1];
  memset(output, 'X', Dmax);
  output[Dmax] = 0;
#endif
  for (uint8_t i = 0; i < N; i++)
  {
    uint32_t new_gain;
    uint8_t new_lab;
#ifdef DEBUG
    char new_output[Dmax];
    memset(new_output, 0, Dmax);
    solve(exps + i, &new_gain, &new_lab, 0, Dmax, new_output);
#else
    solve(exps + i, &new_gain, &new_lab, 0, Dmax);
#endif
    if (new_gain > max_gain)
    {
      max_gain = new_gain;
      max_lab = new_lab;
#ifdef DEBUG
      memset(output, 'X', Dmax);
      for (uint8_t i = 0; i < Dmax; i++)
      {
        if (new_output[i])
        {
          output[i] = new_output[i];
        }
      }
#endif
    }
    else if (new_gain == max_gain)
    {
      if (new_lab > max_lab)
      {
        max_lab = new_lab;
#ifdef DEBUG
        memset(output, 'X', Dmax);
        for (uint8_t i = 0; i < Dmax; i++)
        {
          if (new_output[i])
          {
            output[i] = new_output[i];
          }
        }
#endif
      }
    }
  }

  printf("%u %u\n", max_gain, max_lab);
#ifdef DEBUG
  printf("%s\n", output);
#endif
}
