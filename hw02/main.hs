import           Control.DeepSeq ( rnf )
import           Control.Monad.State
import           Control.Parallel
import           Control.Parallel.Strategies
import           Data.Bits
import qualified Data.ByteString.Char8 as B
import           Data.List
import           Data.Maybe
import           Data.Word

type Lab = Word64
type LabFill = Word8
type Days = Int

type Experiment = (Int, Lab)

type Scanner = State [B.ByteString]

str :: Scanner B.ByteString
str = do
  s <- get
  let h : t = s
  put t
  return h

int :: Scanner Int
int = fst . fromJust . B.readInt <$> str

experiment :: Scanner Experiment
experiment = do
  n <- int
  g <- int
  a <- replicateM n int
  return (g, foldr1 (.|.) . map (shiftL 1) $ a)

input :: Scanner (Days, [Experiment])
input = do
  n    <- int
  days <- int
  exps <- replicateM n experiment
  return (days, exps)

newtype LabResult = MkLabResult (Int, LabFill) deriving (Eq, Ord) -- Gain, Lab fill

instance NFData LabResult where
  rnf (MkLabResult (g, l)) = rnf g `seq` rnf l

labSpan :: Experiment -> Int
labSpan (_, a) = 64 - countLeadingZeros a

instance Show LabResult where
  show (MkLabResult (g, a)) = show g ++ " " ++ show a ++ "\n"

labNeutral :: Lab -> LabResult
labNeutral l = MkLabResult (0, fromIntegral . popCount $ l)

shiftE :: Experiment -> Experiment
shiftE = fmap (`shiftL` 1)

allShifts :: Days -> Experiment -> [Experiment]
allShifts days e =
  let s' = take (days - labSpan e + 1) . iterate shiftE $ e in s'

fits :: Lab -> Experiment -> Bool
fits lab exp = snd exp .&. lab == 0

safeMaximum :: Ord a => [a] -> Maybe a
safeMaximum [] = Nothing
safeMaximum l  = Just $ maximum l

solve :: Lab -> Days -> [Experiment] -> LabResult
solve lab _    []             = labNeutral lab
solve lab days (me : minions) = solution
 where
  allCombs     = filter (fits lab) . allShifts days $ me
  subsolutions = map (\(_, a) -> solveAll (a .|. lab) days minions) $ allCombs
  solution     = fromMaybe (labNeutral lab) $ addMe <$> safeMaximum subsolutions
  addMe (MkLabResult (g, l)) = MkLabResult (g + fst me, l)

retail :: [a] -> [[a]]
retail = (:) [] . takeWhile (not . null) . iterate tail

solveAll :: Lab -> Days -> [Experiment] -> LabResult
solveAll lab _ [] = labNeutral lab
solveAll lab days exps =
  let partialSols' = map (solve lab days) . retail $ exps
      partialSols  = partialSols' `using` parList rdeepseq
  in  maximum partialSols

main :: IO ()
main =
  B.interact
    $ B.pack
    . show
    . uncurry (solveAll 0)
    . fmap (sortBy labSorter)
    . evalState input
    . B.words
  where labSorter e e' = labSpan e' `compare` labSpan e

