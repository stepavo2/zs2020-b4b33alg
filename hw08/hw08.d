private struct Pos
{
  short x, y;
  uint cost;

  int opCmp(ref const Pos rhs) const
  {
    return cost - rhs.cost;
  }
}

void main()
{
  import std.stdio : readf, writeln;
  import std.bitmanip : BitArray;
  import std.array : uninitializedArray;
  import std.range : retro;
  import std.algorithm.comparison : max, min;
  import std.container : heapify;
  import core.memory : GC;

  /* GC.disable; */

  ubyte width, height, cnum;
  uint fast_cost, slow_cost;
  readf("%d %d\n%d %d\n%d\n", &width, &height, &fast_cost, &slow_cost, &cnum);

  ubyte x, y, x2, y2;
  readf("%d %d %d %d\n", &x, &y, &x2, &y2);
  immutable ubyte extend_hor = cast(ubyte)(x2 - x);
  immutable ubyte extend_vert = cast(ubyte)(y2 - y);
  width -= extend_hor;
  height -= extend_vert;

  // grid[height - 1][width - 1]
  auto invalid_raw = new ulong[2][](height);
  auto slow_raw = new ulong[2][](height);
  auto invalid = uninitializedArray!(BitArray[])(height);
  auto slow = uninitializedArray!(BitArray[])(height);

  auto costs = new uint[][](height, width);
  foreach (ref a; costs)
  {
    a[] = cast(uint)(-1);
  }

  foreach (i; 0 .. height)
  {
    invalid[i] = BitArray(invalid_raw[i], width);
    slow[i] = BitArray(slow_raw[i], width);
    slow[i][0] = true;
    slow[i][width - 1] = true;
  }
  slow[0][] = true;
  slow[height - 1][] = true;

  foreach (_; 0 .. cnum - 1)
  {
    ubyte x1, y1;
    readf("%d %d %d %d\n", &x1, &y1, &x2, &y2);
    foreach (i; max(0, y1 - extend_vert) .. min(y2 + 1, height))
    {
      invalid[i][max(0, x1 - extend_hor) .. min(x2 + 1, width)] = true;
    }
    foreach (i; max(0, y1 - extend_vert - 1) .. min(y2 + 2, height))
    {
      slow[i][max(0, x1 - extend_hor - 1) .. min(x2 + 2, width)] = true;
    }
  }

  ubyte target_x, target_y;
  readf("%d %d", &target_x, &target_y);

  Pos[] posBuffer = [];
  auto queue = heapify!"a > b"(posBuffer);
  immutable Pos[4] deltas = [Pos(0, -1), Pos(-1, 0), Pos(1, 0), Pos(0, 1)];
  costs[y][x] = 0;
  queue.insert(Pos(x, y, 0));
  bool found = false;

  toplevel: while (!queue.empty)
  {
    auto cur = queue.front;
    queue.removeFront();
    foreach (ref d; deltas)
    {
      immutable next_x = cur.x + d.x;
      if (next_x < 0 || next_x >= width)
      {
        continue;
      }

      immutable next_y = cur.y + d.y;
      if (next_y < 0 || next_y >= height)
      {
        continue;
      }

      if (invalid[next_y][next_x])
      {
        continue;
      }

      uint* prev_next_step_cost = &costs[next_y][next_x];
      immutable is_slow = slow[cur.y][cur.x] || slow[next_y][next_x];
      uint next_step_cost = (is_slow ? slow_cost : fast_cost) + costs[cur.y][cur.x];
      if (*prev_next_step_cost > next_step_cost)
      {

        if (next_x == target_x && next_y == target_y)
        {
          next_step_cost.writeln;
          found = true;
          break toplevel;
        }

        *prev_next_step_cost = next_step_cost;
        queue.insert(Pos(cast(short)(next_x), cast(short)(next_y), next_step_cost));
      }
    }
  }

  if (!found)
  {
    writeln("-1");
  }
}
